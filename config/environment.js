// Std library imports
const process = require('process');
const path = require('path');

// Additional packages
const PouchDB = require('pouchdb');

// Helper functions
const rootPath = path.resolve(process.env.NODE_PATH);
const appEnvironment = process.env.NODE_ENV || 'development'

// Exports
const db = new PouchDB(path.resolve(rootPath, 'db', appEnvironment));
const requireRelative = (relativePath) => require(path.resolve(rootPath, relativePath))

exports.db = db;
exports.requireRelative = requireRelative;
