# Sort'd - Coding Challenge

## Pre-challenge setup

To get started, run `bin/setup`.

This will get you setup with:
- latest NodeJS LTS
- the dependencies to build and run the basic project

You can verify that everything has been installed and working correctly
with `yarn run test *` and you should have a passing test as shown
below.

```shell
PASS  src/App.test.js
✓ renders without crashing (23ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        0.92s, estimated 1s
```

At this point STOP. And we'll continue with the context of the test
onsite.


## On-site Challenge Notes

### Want to build a NodeJS API? This might help you get started.

If you wish to write the API server in NodeJS, we've already provided a
basic setup that will allow you to fetch the data from a
[pouchdb](https://pouchdb.com) database.

Part of the setup will create the development database, which can then
be initialised with:

```
const PouchDB = require('pouchdb');
const db = new PouchDB('db/development');
```

and read with

```
const doc = await db.get('1234')
```

and written to with

```
await db.put({
  _id: '1234',
  ...doc
});
```
