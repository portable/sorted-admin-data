import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, ReactReduxContext } from 'react-redux'
import configureStore, { history } from './configureStore'
import App from './App';

import './index.scss';

const store = configureStore()

it('renders without crashing', () => {
  const div = document.createElement('div');
  const app = () =>
    <Provider store={store} context={ReactReduxContext}>
      <App history={history} context={ReactReduxContext} />
    </Provider>

  ReactDOM.render(app(), div);
  ReactDOM.unmountComponentAtNode(div);
});
