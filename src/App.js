import React from 'react';
import { Switch, Route } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';

import styles from './App.module.scss';

import Welcome from './components/Welcome';
import NotFound from './components/NotFound';

const App = ({history, context}) => {
  return (
    <div className={styles.container}>
      <h1 className={styles.h1}>SORT'D</h1>
      <ConnectedRouter history={history} context={context}>
        <Switch>
          <Route exact path="/" render={Welcome} />
          <Route render={NotFound} />
        </Switch>
      </ConnectedRouter>
    </div>
  );
}

export default App;
