import React from 'react';

const Welcome = () => {
  return (
    <div>
      <h1>Welcome to Sort'd</h1>
      <p>Every day, small injustices happen to you.</p>
      <p>We're here to help get them sorted. You'll need 5 minutes, and the email address of the person who's made you mad.</p>
      <p>You tell what happened, how you want to fix it. We work some magic to deliver the message to them and get an outcome sorted.</p>
      <p>Throw that passive aggressive post-it note in the recycling bin and...</p>
    </div>
  )
};

export default Welcome;
