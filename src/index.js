import React from 'react';
import ReactDOM from 'react-dom';

import { Provider, ReactReduxContext } from 'react-redux'
import configureStore, { history } from './configureStore'

import './index.scss';
import App from './App';

const store = configureStore()

const app = () => {
  return (
    <Provider store={store} context={ReactReduxContext}>
      <App history={history} context={ReactReduxContext} />
    </Provider>
  );
}

ReactDOM.render(app(), document.getElementById('root'));
