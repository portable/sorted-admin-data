const { db, requireRelative } = require('../../config/environment');

const claims = requireRelative('db/fixtures/data/index');

(async() => {
  const info = await db.info();
  console.log('INSERTING...');
  try {
    for (let c of claims){
      await db.put(c);
    }
  } catch(e){
    console.log(e);
  }
  console.log('...AND DONE');
})();
