const { db } = require('../../config/environment');

(async() => {
  const docs = await db.allDocs();

  const data = await Promise.all(
    docs.rows.map(row => db.get(row.id))
  );

  console.log(JSON.stringify(data,null,2));
})();
