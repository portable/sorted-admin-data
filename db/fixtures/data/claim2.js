module.exports = {
  _id: '1235',
  applicant: {
    firstname: 'Jeffery',
    surname: 'Smert',
    email: 'jsmert@example.com.au',
  },
  respondent: {
    firstname: 'Luke',
    surname: 'Derp',
    email: 'sderp@example.com.au',
  },
  details: {
    type: 'home',
    what: 'Luke put the Tupperware in the wrong place',
    why: 'I had to repack it and I was already busy',
    toneCopWarning: [0.2, 0.2],
  },
  negotiations: [
    {
      from: 'applicant',
      date: '2019-04-09-11:24',
      asks: [
        {
          type: 'task',
          amount: 'dishes',
          duration: '1 week'
        },
        {
          type: 'apology'
        }
      ],
      toneCopWarning: [0.6,0.1],
      justification: 'You drive me crazy'
    },
    {
      from: 'respondent',
      date: '2019-04-09-11:46',
      asks: [
        {
          type: 'apology'
        }
      ],
      toneCopWarning: [0.4,0.1],
      justification: 'Happy to apologise, but I do the dishes all the time anyhow'
    },
    {
      from: 'applicant',
      date: '2019-04-10-08:12',
      asks: [
        {
          type: 'task',
          amount: 'dishes',
          duration: '3 days'
        },
        {
          type: 'apology'
        }
      ],
      toneCopWarning: [0.5, 0.5],
      justification: 'You do not!'
    },
  ],
}