module.exports = {
  _id: '1240',
  applicant: {
    firstname: 'Harry',
    surname: 'Potter',
    email: 'harry.potter@portable.com.au',
  },
  respondent: {
    firstname: 'Lord',
    surname: 'Voldemort',
    email: 'tom.riddle@portable.com.au',
  },
  details: {
    type: 'workplace',
    what: 'Tom has been making various attempts on my life',
    why: 'It makes it difficult to concentrate on my school studies',
    toneCopWarning: [0.2, 0.2],
  },
  negotiations: [
    {
      from: 'applicant',
      date: '2019-03-12-13:15',
      asks: [
        {
          type: 'money',
          amount: '50',
        }
      ],
      toneCopWarning: [0.1, 0.1],
      justification: 'I need to buy some Butterbeer to cheer me up'
    },
    {
      from: 'respondent',
      date: '2019-03-14-09:27',
      asks: [
        {
          type: 'money',
          amount: '5',
        }
      ],
      toneCopWarning: [0.5, 0.2],
      justification: 'You may have one Butterbeer before I catch up with you, but no more'
    }
  ],
}
