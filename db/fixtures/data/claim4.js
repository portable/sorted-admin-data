module.exports = {
  _id: '1237',
  applicant: {
    firstname: 'Cameron',
    surname: 'Jones',
    email: 'cameron@example.com.au',
  },
  respondent: {
    firstname: 'Suzie',
    surname: 'Dobson',
    email: 'suzie@example.com.au',
  },
  details: {
    type: 'home',
    what: 'Suzie has been making fun of my clothes, pointing out every little stain, or when things don\'t match',
    why: 'I\'m doing the best I can, and to be honest, it\'s really none of her business. It makes me feel small',
    toneCopWarning: [0.4, 0.4],
  },
  negotiations: [
    {
      from: 'applicant',
      date: '2019-04-06-14:43',
      asks: [
        {
          type: 'apology',
        }
      ],
      toneCopWarning: [0.2, 0.2],
      justification: 'Just say sorry. And stop it. But definately say sorry'
    },
    {
      from: 'respondent',
      date: '2019-04-06-18:43',
      asks: [
        {
          type: 'nothing',
        }
      ],
      toneCopWarning: [0.9, 0.7],
      justification: 'Get over it idiot'
    },
    {
      from: 'applicant',
      date: '2019-04-06-19:43',
      asks: [
        {
          type: 'apology',
        }
      ],
      toneCopWarning: [0.8, 0.3],
      justification: 'Come on Suzie you\'re making it worse'
    },
    {
      from: 'respondent',
      date: '2019-04-06-19:52',
      asks: [
        {
          type: 'nothing',
        }
      ],
      toneCopWarning: [0.7, 0.7],
      justification: 'Go to hell'
    },
    {
      from: 'applicant',
      date: '2019-04-06-20:52',
      asks: [
        {
          type: 'apology',
        }
      ],
      toneCopWarning: [0.5, 0.6],
      justification: 'Why are you being such an asshole?'
    },
    {
      from: 'respondent',
      date: '2019-04-06-20:57',
      asks: [
        {
          type: 'nothing',
        }
      ],
      toneCopWarning: [0.3, 0.3],
      justification: 'hahahaha'
    },
  ],
}
