module.exports = {
  _id: '1234',
  applicant: {
    firstname: 'Andrew',
    surname: 'Fulton',
    email: 'andrew.fulton@portable.com.au',
  },
  respondent: {
    firstname: 'Allison',
    surname: 'Snow',
    email: 'allison@portable.com.au',
  },
  details: {
    type: 'workplace',
    what: 'Allison has been leaving empty coffee cups around the office, especially in meeting rooms',
    why: 'I have to clean them up and it makes us look bad in front of clients',
    toneCopWarning: [0.2, 0.2],
  },
  negotiations: [
    {
      from: 'applicant',
      date: '2019-03-12-13:15',
      asks: [
        {
          type: 'money',
          amount: '90',
        }
      ],
      toneCopWarning: [0.1, 0.1],
      justification: 'Last time I spilled cold coffe down my shirt and need to get a new one'
    },
    {
      from: 'respondent',
      date: '2019-03-14-09:27',
      asks: [
        {
          type: 'money',
          amount: '30',
        }
      ],
      toneCopWarning: [0.5, 0.2],
      justification: 'Dude it\'s not that nice a shirt'
    }
  ],
}
