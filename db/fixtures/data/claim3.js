module.exports = {
  _id: '1236',
  applicant: {
    firstname: 'Wendy',
    surname: 'Mclean',
    email: 'wendy@example.com.au',
  },
  respondent: {
    firstname: 'Andrew',
    surname: 'Fulton',
    email: 'andrew@example.com.au',
  },
  details: {
    type: 'home',
    what: 'You never get off your phone when I am talking to you.',
    why: 'It\'s incredibly rude, like you are not paying any attention to me at all',
    toneCopWarning: [0.7, 0.3],
  },
  negotiations: [
    {
      from: 'applicant',
      date: '2019-03-12-13:15',
      asks: [
        {
          type: 'money',
          amount: '90',
        }
      ],
      toneCopWarning: [0.7, 0.2],
      justification: 'Im sick of it'
    }
  ],
}
