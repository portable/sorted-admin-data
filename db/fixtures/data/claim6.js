module.exports = {
  _id: '1239',
  applicant: {
    firstname: 'Jimmy',
    surname: 'Jorts',
    email: 'jimmy@example.com',
  },
  respondent: {
    firstname: 'David',
    surname: 'Ohern',
    email: 'david@example.com',
  },
  details: {
    type: 'workplace',
    what: 'Why do you hate me?',
    why: 'You\'re a real asshole',
    toneCopWarning: [0.5, 0.5],
  },
  negotiations: [
    {
      from: 'applicant',
      date: '2019-05-1-09:15',
      asks: [
        {
          type: 'money',
          amount: '50',
        }
      ],
      toneCopWarning: [0.5, 0.5],
      justification: 'Pay up idiot'
    },
    {
      from: 'respondent',
      date: '2019-05-1-09:20',
      asks: [
        {
          type: 'nothing'
        }
      ],
      toneCopWarning: [0.4, 0.3],
      justification: 'What is this? What are you talking about?'
    },
    {
      from: 'applicant',
      date: '2019-05-1-09:25',
      asks: [
        {
          type: 'apology'
        }
      ],
      toneCopWarning: [0.6, 0.6],
      justification: 'You\'re an asshole, asshole. Apologise.'
    }
  ],
} 
