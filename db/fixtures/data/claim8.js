module.exports = {
  _id: '1241',
  applicant: {
    firstname: 'Nathan',
    surname: 'barley',
    email: 'nathan@example.com',
  },
  respondent: {
    firstname: 'Bryan',
    surname: 'Gould',
    email: 'bryan@example.com',
  },
  details: {
    type: 'home',
    what: 'Music is too loud all the goddamn time, your\'re up too late and I can\'t sleep.',
    why: 'This is my house too, I want to be able to live in it.',
    toneCopWarning: [0.3, 0.26],
  },
  negotiations: [
    {
      from: 'applicant',
      date: '2019-05-01-13:15',
      asks: [
        {
          type: 'apology'
        },
        {
          type: 'task',
          amount: 'laundry',
          duration: '1 week'
        },
        {
          type: 'task',
          amount: 'dishes',
          duration: '1 week'
        }
      ],
      toneCopWarning: [0.2, 0.2],
      justification: 'be good if you could life your game in general mate.'
    }
  ],
}
