module.exports = {
  _id: '1238',
  applicant: {
    firstname: 'Laura',
    surname: 'Mitchell',
    email: 'laura@example.com',
  },
  respondent: {
    firstname: 'Samson',
    surname: 'White',
    email: 'samson@example.com',
  },
  details: {
    type: 'workplace',
    what: 'Samson talks over me in meetings. He never lets me finish, barely even looks at me.',
    why: 'I\'m working hard and have a lot of good ideas, but don\'t get a chance to get the rest of the team involved',
    toneCopWarning: [0.34, 0.1],
  },
  negotiations: [
    {
      from: 'applicant',
      date: '2019-05-01-13:15',
      asks: [
        {
          type: 'apology'
        },
        {
          type: 'task',
          amount: 'coffee',
          duration: '3 days'
        }
      ],
      toneCopWarning: [0.2, 0.2],
      justification: 'Really I just want the apology, but buying me coffee for a couple of days might be a nice gesture.'
    },
    {
      from: 'respondent',
      date: '2019-05-11-09:37',
      asks: [
        {
          type: 'nothing',
        }
      ],
      toneCopWarning: [0.5, 0.3],
      justification: 'Laura this is very inappropriate. If you have a grievance you should be talking to HR, not sending me messages on whatever this is'
    },
    {
      from: 'applicant',
      date: '2019-05-11-10:37',
      asks: [
        {
          type: 'apology',
        }
      ],
      toneCopWarning: [0.2, 0.2],
      justification: 'Come on Samson you do this all the time. I don\'t want to get HR involved, I just want you to recognize your behavior.'
    },
  ],
}
